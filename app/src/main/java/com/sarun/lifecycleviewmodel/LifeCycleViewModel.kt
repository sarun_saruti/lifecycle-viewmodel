package com.sarun.lifecycleviewmodel

import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

abstract class LifeCycleViewModel(
    private val lifeCycle: Lifecycle,
    val savedStateHandle: SavedStateHandle
) : ViewModel() {
    fun launchWhenCreated(block: suspend CoroutineScope.() -> Unit): Job = viewModelScope.launch {
        lifeCycle.whenCreated(block)
    }

    fun launchWhenStarted(block: suspend CoroutineScope.() -> Unit): Job = viewModelScope.launch {
        lifeCycle.whenStarted(block)
    }

    fun launchWhenResumed(block: suspend CoroutineScope.() -> Unit): Job = viewModelScope.launch {
        lifeCycle.whenResumed(block)
    }

    fun launch(block: suspend CoroutineScope.() -> Unit): Job = viewModelScope.launch {
        viewModelScope.launch(block = block)
    }
}